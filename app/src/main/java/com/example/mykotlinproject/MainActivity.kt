package com.example.mykotlinproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val nimEt = findViewById<EditText>(R.id.nimEt)
        val namaEt = findViewById<EditText>(R.id.namaEt)
        val nilaiEt = findViewById<EditText>(R.id.nilaiEt)
        val prosesBtn = findViewById<Button>(R.id.prosesBtn)

        prosesBtn.setOnClickListener {
            var nim = nimEt.text.toString()
            var nama = namaEt.text.toString()
            var nilai = nilaiEt.text.toString()

            intent = Intent(this@MainActivity,MainActivityKedua::class.java)
            intent.putExtra("NIM",nim)
            intent.putExtra("NAMA",nama)
            intent.putExtra("NILAI",nilai)
            startActivities(arrayOf(intent))
        }
    }
}
