package com.example.mykotlinproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MainActivityKedua : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_kedua)

        var intent = intent
        val nim = intent.getStringExtra("NIM")
        val nama = intent.getStringExtra("NAMA")
        val nilai = intent.getStringExtra("NILAI")

        val resultTv = findViewById<TextView>(R.id.resultTV)
        val resultTv2 = findViewById<TextView>(R.id.resultTV2)

        resultTv.text = "NIM              : "+nim+"\nNama           : "+nama+"\nNilai Angka : "+nilai
       if (nilai>= 80.toString()){
            resultTv2.text = "keterangan : A"
        }else if (nilai>= 60.toString()){
           resultTv2.text = "keterangan : B"
        }else if (nilai>= 40.toString()){
           resultTv2.text = "keterangan : C"
        }else if (nilai>= 20.toString()){
           resultTv2.text = "keterangan : D"
        }else if (nilai>= 0.toString()){
           resultTv2.text = "keterangan : E"
        }
    }
}
